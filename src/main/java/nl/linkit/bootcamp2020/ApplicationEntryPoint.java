package nl.linkit.bootcamp2020;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class ApplicationEntryPoint {
    static KafkaProducer<String, String> producer;

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            throw new IllegalArgumentException("Invalid command line, exactly two argument required");
        }

        String filename = args[0];
        String ip = args[1];
        Properties props = new Properties();
        props.put("bootstrap.servers", ip + ":9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<>(props);
        Path folder = Paths.get("/opt/data");
        final Path inputPath = folder.resolve(filename);
        Files.lines(inputPath).parallel().forEach(line -> producer.send(new ProducerRecord<>("zips", line)));
        producer.close();
    }
}
